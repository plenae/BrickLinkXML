# BrickLinkXML
This project contains a small script which converts a CSV list of Lego parts to the XML format BrickLink can import in the "copy-paste wanted list import screen": https://www.bricklink.com/v2/wanted/upload.page

## Installation
Requires python3 and a (very) small lib of python snippets called ['snips-plenae'](https://codeberg.org/plenae/python-snips).

Installing in a virtual environment named venv:
```
python -m venv venv
source venv/bin/activate
pip install wheel
pip install git+https://codeberg.org/plenae/python-snips.git
pip install git+https://codeberg.org/plenae/BrickLinkXML.git
```

This installs the required packages in the virtual environment packages and a script csv_to_bricklinkxml in the venv's bin directory.


## Usage
Activate your venv by sourcing it's activate script: `$ source venv/bin/activate`. This puts your venv's bin folder at the front of your shell path. The bin dir contains a csv_to_bricklinkxml executable python script, calling bricklinkxml.csv_to_bricklinkxml.py.

```
$ csv_to_bricklinkxml --help
usage: csv_to_bricklinkxml [-h] colourlib language shoppinglist outfile

positional arguments:
  colourlib     CSV list of colours in the lang of the shopping list.
  language      Language key in the colourlib matching the language used in
                the shopping list.
  shoppinglist  List of blocks to transform to XML
  outfile       Write bricklist XML to file.

optional arguments:
  -h, --help    show this help message and exit
```

## Workflow
[Refer to the wiki for help on how to put csv_to_bricklinkxml to use.](/plenae/BrickLinkXML/wiki/Using-csv_to_bricklinkxml) Basically you will:
* Scan and OCR a list of parts from a book to PDF.
* Extract a CSV from the PDF using for example the fantastic Tabula software. https://github.com/tabulapdf/tabula
* Check the CSV you get from tabula.
* Check colours.
* Convert your scanned list to a bricklick XML.
* Try and upload your XML.
* Correct your CSV with BrickLink part numbers you had wrong and re-upload.
* Iterate until your whole list is in BrickLink.

## Future Features or What we don't have (for now?)
* A nice colourlib in multiple languages.
* Easily configurable column headers.
* A GUI for people who don't know how to use CLI, python, XML, etc

## Refer to
* https://www.bricklink.com/help.asp?helpID=207 for XML format help. I thinkt this format might be brickstock. Or not. I don't now. It doesn't seem to be a standard. I'm sure Lego people understand standards though :-)
* https://rebrickable.com for a very nice cross-reference of part numbers
* The BrickLink API should be fun in the future: https://www.bricklink.com/v2/api/welcome.page
* I found some info on previous work here:
  * https://forum.brickset.com/discussion/24682/converting-xls-lego-inventory-to-brickstock-or-bricklink-xml-file
  * http://klatusklutter.blogspot.com/2014/08/blog-post.html#
  * I didn't see the sources so I didn't investigate.

## Disclaimer & License
* I don't know the first thing about Lego or the universe of communities of people working around Lego. Thanks for understanding.
* I'm intentionally releasing this under a nice and aggressive GPLv3+ so this remains free for eternity.
