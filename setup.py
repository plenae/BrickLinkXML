# setuptools setup.py for bricklinkXML
#
# Copyright (C) 2020  Pieter Lenaerts

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="BrickLinkXML",
    version="0.1",
    packages=find_packages(),
    entry_points={
        'console_scripts':
        ['csv_to_bricklinkxml=bricklinkxml.csv_to_bricklinkxml:main'],
    },

    install_requires=["lxml", "snips-plenae"],

    # metadata to display on PyPI
    author="Pieter Lenaerts",
    author_email="pieter.aj.lenaerts@gmail.com",
    description="Script and lib to convert CSV list of Lego parts to " + \
    "BrickLink XML import format",
    long_description=long_description,
    long_description_content_type="text/markdown",
    keywords="bricklink lego brickstock",
    url="https://codeberg.org/plenae/BrickLinkXML",
    project_urls={
        "Bug Tracker": "https://codeberg.org/plenae/BrickLinkXML/issues",
        "Documentation": "https://codeberg.org/plenae/BrickLinkXML/",
        "Source Code": "https://codeberg.org/plenae/BrickLinkXML/",
    },
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License v3 or " +
        "later (GPLv3+)",
    ]
)
