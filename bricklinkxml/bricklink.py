# BrickLinck datamodel for converting CSV wanted Lego part list to BrickLinkXML
#
# Copyright (C) 2020  Pieter Lenaerts

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

from lxml.etree import Element, SubElement, ElementTree


class Inventory():
    """
    A list of item/colour combinations with a quantity

    One attributes:
        * items = dict with a tuple (item id, colour id) as key and an item
          object as value.

    Functions:
        * add_item(self, item): check if the combination item id, colour id is
          in the inventory. If it is, then increase the quantity of the
          existing Item, else add a new Item.
    """
    def add_item(self, item):
        if not hasattr(self, 'items'):
            self.items = dict()
        if (item.id, item.colour.id) in self.items:
            self.items[(item.id, item.colour.id)].qty += item.qty
        else:
            self.items[(item.id, item.colour.id)] = item


class Item():
    def __init__(self, id, colour, qty):
        self.id = id
        self.colour = colour
        self.qty = qty
        self.itemtype = 'P'
        self.max_price = -1.0
        self.condition = 'X'
        self.notify = 'N'


class Colour():
    def __init__(self, name, id):
        """Colour name in lang of input file."""
        self.name = name
        self.id = id


def get_bricklinkxml_elementtree(inv):
    """Convert an Inventory to an XML ElementTree object"""
    inv_el = Element('INVENTORY')

    for it in inv.items.values():
        el = SubElement(inv_el, 'ITEM')
        item_type = SubElement(el, 'ITEMTYPE')
        item_type.text = 'P'
        item_id = SubElement(el, 'ITEMID')
        item_id.text = str(it.id)
        colour = SubElement(el, 'COLOR')
        colour.text = str(it.colour.id)
        qty = SubElement(el, 'MINQTY')
        qty.text = str(it.qty)
    return ElementTree(inv_el)
