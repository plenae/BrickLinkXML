# CLI script converting CSV wanted Lego part list to BrickLinkXML
#
# Copyright (C) 2020  Pieter Lenaerts

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import bricklinkxml.io
import csv
from snips.argparse_actions import FileNewlineType
import argparse
import sys


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('colourlib',
                        type=FileNewlineType(mode='r', newline=''),
                        help='CSV list of colours in the lang of the ' +
                             'shopping list.')
    parser.add_argument('language', help='Language key in the colourlib ' +
                        'matching the language used in the shopping list.')
    parser.add_argument('shoppinglist',
                        type=FileNewlineType(mode='r', newline=''),
                        help='List of blocks to transform to XML')
    parser.add_argument('outfile', type=argparse.FileType(mode='wb'),
                        default=sys.stdout,
                        help='Write bricklist XML to file.')
    args = parser.parse_args()

    colour_reader = csv.DictReader(args.colourlib)
    try:
        lib = bricklinkxml.io.get_colourlib(colour_reader, args.language)
    except KeyError as e:
        print('Could not read colourlib CSV: ' + str(e))
        exit(1)

    shop_reader = csv.DictReader(args.shoppinglist)
    try:
        inv = bricklinkxml.io.get_inventory(shop_reader, lib)
    except KeyError as e:
        print('Could not read shoppinglist CSV: ' + str(e))
        exit(1)

    inv_et = bricklinkxml.bricklink.get_bricklinkxml_elementtree(inv)
    inv_et.write(args.outfile, pretty_print=True)


if __name__ == '__main__':
    main()
