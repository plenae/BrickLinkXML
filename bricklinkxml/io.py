# IO functions for converting CSV wanted Lego part list to BrickLinkXML
#
# Copyright (C) 2020  Pieter Lenaerts

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

from bricklinkxml.bricklink import Colour, Inventory, Item
import logging


def get_colourlib(reader, lang):
    """Read each record in colourlib csv dictreader and return a dict of
    colours.

    Params:
        - reader: csv dictreader on the colourlib csv file
        - lang: the language to use as key

    Return: a dict with
        - key = colour name in lang,
        - values = brickstock.brickstock.Colour instances.
    """
    if not ('ID' in reader.fieldnames and lang in reader.fieldnames):
        raise KeyError('\'ID\' or \'' + lang +
                       '\' columns not found in colourlib CSV!')
    colourlib = dict()
    for line in reader:
        lang_key = line[lang].lower().strip()
        colour_id = int(line['ID'].strip())
        if lang_key:
            colourlib[lang_key] = Colour(lang_key, colour_id)
    return colourlib


def get_inventory(reader, colourlib):
    """Return inventory object with items."""
    for f in ['Colour', 'Element', 'Quantity']:
        if f not in reader.fieldnames:
            raise KeyError('Column \'' + f +
                           '\' not found in wanted list CSV!')
    inv = Inventory()
    for line in reader:
        try:
            colour_name = line['Colour'].lower().strip()
            colour_id = colourlib[colour_name].id
            item_id = line['Element'].lower().strip()
            qty = int(line['Quantity'].strip())
            item = Item(item_id, Colour(colour_name, colour_id), qty)
            inv.add_item(item)
        except KeyError:
            logging.warning('Colour ' + colour_name +
                            ' not found. Skipping line.')
    return inv
